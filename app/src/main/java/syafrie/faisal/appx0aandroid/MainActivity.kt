package syafrie.faisal.appx0aandroid

import android.app.DatePickerDialog
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray

class MainActivity : AppCompatActivity(), View.OnClickListener {
	lateinit var mhsAdapter : AdapterDataMhs
	var daftarMhs = mutableListOf<HashMap<String,String>>()
	val url = "http://192.168.43.66/app-x0a-web/show_data.php"


	var tahun = 0
	var bulan = 0
	var hari = 0
	var jam = 0
	var menit = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
		mhsAdapter = AdapterDataMhs(daftarMhs)
		btnTgl.setOnClickListener(this)
		listMhs.layoutManager = LinearLayoutManager(this)
		listMhs.adapter = mhsAdapter
		showDataMhs()
    }
	var dateChangeDialog = DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
		tahun = year
		bulan = month+1
		hari = dayOfMonth
	}

	override fun onCreateDialog(id: Int): Dialog {
		when(id){

			20 -> return DatePickerDialog(this, dateChangeDialog, tahun,bulan,hari)

		}
		return super.onCreateDialog(id)
	}
	override fun onClick(v: View?) {
		when(v?.id){

			R.id.btnTgl -> showDialog(20)
		}
	}
	fun showDataMhs() {
		val request = StringRequest(
			Request.Method.POST, url,
			Response.Listener { response ->
				val jsonArray = JSONArray(response)
				for (x in 0..(jsonArray.length() - 1)) {
					val jsonObject = jsonArray.getJSONObject(x)
					var mhs = HashMap<String, String>()
					mhs.put("nim", jsonObject.getString("nim"))
					mhs.put("nama", jsonObject.getString("nama"))
					mhs.put("nama_prodi", jsonObject.getString("nama_prodi"))
					mhs.put("url", jsonObject.getString("url"))
					mhs.put("jeniskel", jsonObject.getString("jeniskel"))
					mhs.put("tgllahir", jsonObject.getString("tgllahir"))
					mhs.put("alamat", jsonObject.getString("alamat"))
					daftarMhs.add(mhs)
				}
				mhsAdapter.notifyDataSetChanged()
			},
			Response.ErrorListener { error ->
				Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG)
					.show()
			})
		val queue = Volley.newRequestQueue(this)
		queue.add(request)

	}
}
